import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {DetailsPage} from "../details/details";
import * as AssuranceMap from '../../models/assurance.model';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  assuranceData : AssuranceMap.Assurance[]
  nom: string;
  prenom: string;
  age: number;
  constructor(public navCtrl: NavController) {
    this.assuranceData = AssuranceMap.assuranceMock;

  }

  onDetails(assurance) {
    this.navCtrl.push(DetailsPage, assurance);
  }

}
