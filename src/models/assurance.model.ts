export interface Assurance {
  nom: string;
  imgPath: string
  lien: string;

}
export const assuranceMock = [
  {
    nom: 'ASCOMA',
    imgPath: 'assets/imgs/ascoma.png',
    lien: 'www.ascoma.com'
  },
  {
    nom: 'ALLIANZ',
    imgPath: 'assets/imgs/allianz.png',
    lien: 'www.allianz-senegal.com'
  },

  {
    nom: 'AXA ASSURANCE',
    imgPath: 'assets/imgs/axa.png',
    lien: 'www.axasenegal.sn'
  },
]
